import re

def loadFile(fnm):
    with open(fnm) as f:
        return f.read()

def htmlTransfer(a):
    a = a.replace(r"&", r"&amp;")
    a = a.replace(r" ", r"&nbsp;")
    a = a.replace(r"<", r"&lt;")
    a = a.replace(r">", r"&gt;")
    # a = a.replace(r"\"", r"&quot;")
    a = a.replace(r"'", r"&apos;")
    return a

def inline(s): # 行内样式转换
    s = htmlTransfer(s)
    s = re.sub(r"\*\*(.+?)\*\*", r"<b>\1</b>",s)
    s = re.sub(r"--(.+?)--", r"<del>\1</del>", s)
    s = re.sub(r"_(.+?)_", r"<i>\1</i>", s)
    s = re.sub(r"`(.+?)`", r"<code>\1</code>", s)
    s = re.sub(r"\\(.)", r"\1", s)
    return s.replace(r"\"", r"&quot;")

def markdown(s):
    s = s.split("\n")
    ret = []
    for i in s:
        tag = 'p'; iret = i
        tmp = iret; iret = re.sub(r"^###### (.+)", r"\1", iret)
        if iret != tmp: tag = 'h6'
        tmp = iret; iret = re.sub(r"^##### (.+)", r"\1", iret)
        if iret != tmp: tag = 'h5'
        tmp = iret; iret = re.sub(r"^#### (.+)", r"\1", iret)
        if iret != tmp: tag = 'h4'
        tmp = iret; iret = re.sub(r"^### (.+)", r"\1", iret)
        if iret != tmp: tag = 'h3'
        tmp = iret; iret = re.sub(r"^## (.+)", r"\1", iret)
        if iret != tmp: tag = 'h2'
        tmp = iret; iret = re.sub(r"^# (.+)", r"\1", iret)
        if iret != tmp: tag = 'h1'
        ret.append("<%s>%s</%s>" % (tag, inline(iret), tag))
    return "\r".join(ret)